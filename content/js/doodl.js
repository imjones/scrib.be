var doodlcanvas, mouseisdown, mirrors, context, x, y, canvasWidth, canvasHeight;
var ongoingTouches = [];
var baseColors = [];
var fullColors = [];
var colIndex = 0;
var showMirrors = false;
var randomColors = false;
var gradient = 100;
var translucency = 255;

function init() {
  mirrors=6;
  doodlcanvas=document.getElementById('doodlcanvas');
  context = doodlcanvas.getContext('2d');

  let allRanges = document.getElementsByClassName('range');
  for(let wrap of allRanges)
  {
    const range = wrap;
    const bubble = wrap.nextSibling;
  
    range.addEventListener("input", () => {
      setBubble(range, bubble);
    });
    setBubble(range, bubble);
  }
  
  // set the correct width and height of the canvas
  // CSS 100% causes it to scale *eyeroll*
  canvasWidth = window.getComputedStyle(doodlcanvas, null).getPropertyValue('width');
  canvasHeight = window.getComputedStyle(doodlcanvas, null).getPropertyValue('height');
  doodlcanvas.setAttribute('width', canvasWidth);
  doodlcanvas.setAttribute('height', canvasHeight);
  canvasWidth = canvasWidth.replace(/px/g, '');
  canvasHeight = canvasHeight.replace(/px/g, '');

  // init base colours :P
  //baseColors.push(new Color(0, 0, 0)); // Bl
  baseColors.push(new Color(255, 0, 0)); // R
  baseColors.push(new Color(255, 255, 0)); // Y
  baseColors.push(new Color(0, 255, 0)); // G
  baseColors.push(new Color(0, 0, 255)); // B
  baseColors.push(new Color(75, 0, 130)); // I
  baseColors.push(new Color(127, 0, 255)); // V
  //baseColors.push(new Color(0, 0, 0)); // Bl
  calculateFullColors();

  setupPallette();

  // events
  var mc = new Hammer(doodlcanvas);
  mc.get('pan').set({ direction: Hammer.DIRECTION_ALL });
  mc.get('pan').set({ threshold: 3 });
  mc.get('pinch').set({ enable: true });
  mc.on("panmove", handlePan);
  mc.on("panstart", function(e) { mouseisdown = true; x=e.offsetX; y=e.offsetY; });

  //doodlcanvas.onmousedown = function(e) { mouseisdown = true; x=e.offsetX; y=e.offsetY; }
  //doodlcanvas.ontouchstart = handleTouchStart;
  //doodlcanvas.onmouseup = function() { mouseisdown = false; }
  //doodlcanvas.ontouchend = handleTouchEnd;
  //doodlcanvas.onmousemove = drawthethingbb;
  //doodlcanvas.ontouchmove = handleTouchMove;
  //doodlcanvas.ontouchcancel = handleTouchCancel;
}

function calculateFullColors() {
  let grads = gradient;
  fullColors = [];
  for(let colLoop=1; colLoop<baseColors.length; colLoop++) {
    let lastCol = baseColors[colLoop-1];
    let currCol = baseColors[colLoop];
    let rGrad = (currCol.r - lastCol.r) / grads;
    let gGrad = (currCol.g - lastCol.g) / grads;
    let bGrad = (currCol.b - lastCol.b) / grads;
    for(let gradLoop=0; gradLoop<=grads; gradLoop++) {
      let actualR = lastCol.r + (rGrad * gradLoop);
      let actualG = lastCol.g + (gGrad * gradLoop);
      let actualB = lastCol.b + (bGrad * gradLoop);
      fullColors.push(new Color(actualR, actualG, actualB, translucency))
    }
  }
}

function handleTouchCancel(e) {
  e.preventDefault();
  console.log("touchcancel.");
  var touches = e.changedTouches;
  
  for (var i = 0; i < touches.length; i++) {
    var idx = ongoingTouchIndexById(touches[i].identifier);
    ongoingTouches.splice(idx, 1);  // remove it; we're done
  }  
}

function ongoingTouchIndexById(idToFind) {
  for (var i = 0; i < ongoingTouches.length; i++) {
    var id = ongoingTouches[i].identifier;
    
    if (id == idToFind) {
      return i;
    }
  }
  return -1;    // not found
}

function handleTouchStart(e) {
  e.preventDefault();
  console.log("touchstart.");
  var touches = e.changedTouches;

  x = touches[0].pageX;
  y = touches[0].pageY;
  
  for (var i = 0; i < touches.length; i++) {
    console.log("touchstart:" + i + "...");
    ongoingTouches.push(copyTouch(touches[i]));
    drawLinesFromCoords(x, y, touches[i].pageX, touches[i].pageY);
    console.log("touchstart:" + i + ".");
  }
}

function handleTouchEnd(e) {
  e.preventDefault();
  console.log("touchend");
  var touches = e.changedTouches;

  for (var i = 0; i < touches.length; i++) {
    var idx = ongoingTouchIndexById(touches[i].identifier);

    if (idx >= 0) {
      drawLinesFromCoords(x, y, touches[i].pageX, touches[i].pageY);
      ongoingTouches.splice(idx, 1);  // remove it; we're done
    } else {
      console.log("can't figure out which touch to end");
    }
  }
}

function handlePan(e) {
  e.preventDefault();
  drawLinesFromCoords(x, y, e.center.x, e.center.y);
  x = e.center.x;
  y = e.center.y;
}

function copyTouch({ identifier, pageX, pageY }) {
  return { identifier, pageX, pageY };
}

function changeMirrors() {
  let mirrorCount = document.getElementById('mirrorCount');
  mirrors = parseInt(mirrorCount.value);
}

function changeGradient() {
  let gradientInput = document.getElementById('gradient');
  gradient = parseInt(gradientInput.value);
  calculateFullColors();
  colIndex = 0;
}

function changeTranslucency() {
  let transInput = document.getElementById('translucency');
  translucency = parseInt(transInput.value);
  calculateFullColors();
  colIndex = 0;
}

function setBubble(range, bubble) {
  const val = range.value;
  bubble.innerHTML = val;
}

function drawMirrors() {
  showMirrors = document.getElementById('showMirrors').value;
}

function drawthethingbb(e) {
  if(mouseisdown) {
    let linesToDraw = getLines(x, y, e.offsetX, e.offsetY);
    linesToDraw.forEach(drawLine);
    x = e.offsetX;
    y = e.offsetY;
  }
  if(showMirrors) {
    
  }
}

function clearScreen() {
  context.clearRect(0, 0, canvasWidth, canvasHeight);
}

function randomizeColors()
{
  for(var colorIndex=0; colorIndex < baseColors.length; colorIndex++)
  {
    var color = baseColors[colorIndex];
    color.r = Math.floor(Math.random() * 255);  
    color.g = Math.floor(Math.random() * 255);  
    color.b = Math.floor(Math.random() * 255);  
  }
  setupPallette();
  calculateFullColors();
}

function updateColor(e) {
  debugger;
  let colId = e.currentTarget.id;
  let picker = e.currentTarget;
  baseColors[colId].r = picker.rgb[0];
  baseColors[colId].g = picker.rgb[1];
  baseColors[colId].b = picker.rgb[2];
  calculateFullColors();
}

function setupPallette() 
{
  var palletteDiv = document.getElementById('pallette');
  palletteDiv.innerHTML = '';
  for(var colLoop=0; colLoop<baseColors.length; colLoop++) {
    var button = document.createElement('input');
    button.id = colLoop;
    button.style = 'width:13px; height:13px; margin-left:8px; border: solid 1px white; cursor: pointer;';
    button.addEventListener('change', updateColor);
    var picker = new jscolor(button, {valueElement:null});
    picker.fromRGB(baseColors[colLoop].r, baseColors[colLoop].g, baseColors[colLoop].b);
    
    palletteDiv.appendChild(button);

  }
}

function getColor() {
  let color = new Color(0,0,0,0);
  if(false) {
    color.r = Math.floor(Math.random() * 255);  
    color.g = Math.floor(Math.random() * 255);  
    color.b = Math.floor(Math.random() * 255);  
    color.a = Math.floor(Math.random() * 255);  
  }
  else {
    color.r = fullColors[colIndex].r;
    color.g = fullColors[colIndex].g;
    color.b = fullColors[colIndex].b;
    color.a = translucency;
    if(randomColors)
      colIndex = Math.floor(fullColors.length * Math.random());
    else
      colIndex = (colIndex + 1) % fullColors.length;
  }
  return 'rgba(' + Math.floor(color.r) + ', ' + Math.floor(color.g) + ', ' + Math.floor(color.b) + ', ' + (translucency/255.0) + ')';  
}

function getHex(n) {
  let retVal = n.toString(16);
  return retVal.length==1 ? '0' + retVal : retVal;
}

function drawLine(line) {
  context.beginPath();
  context.strokeStyle = line.color;
  context.lineWidth = 8;
  context.moveTo(line.p1.x, line.p1.y);
  context.lineTo(line.p2.x, line.p2.y);
  context.stroke();
  context.closePath();  
}

function drawLinesFromCoords(x1, y1, x2, y2) {
  let linesToDraw = getLines(x1, y1, x2, y2);
  linesToDraw.forEach(drawLine);
  x = x2;
  y = y2;
}

class Point {
  constructor(x,y) {
    this.x = x;
    this.y = y;
  }
}

class Line {
  constructor(p1, p2, color) {
    this.p1 = p1;
    this.p2 = p2;
    this.color = color;
  }
}

class Color {
  constructor(r, g, b, a) {
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a;
  }
}

function getLines(x1, y1, x2, y2) {
  let sectors = mirrors * 2;
  let radians = 360 / sectors * (Math.PI / 180);
  let halfWidth = canvasWidth / 2;
  let halfHeight = canvasHeight / 2;
  // Make the centre of the canvas the origin
  x1 -= halfWidth;
  x2 -= halfWidth;
  y1 -= halfHeight;
  y2 -= halfHeight;
  
  var lines = new Array();
  let theColor = getColor();
  for(let sectorLoop=0; sectorLoop<sectors; sectorLoop++) {
    let point1 = new Point((x1 * (Math.cos(radians))) - (y1 * Math.sin(radians)), (x1 * Math.sin(radians)) + (y1 * Math.cos(radians)));
    let point2 = new Point((x2 * (Math.cos(radians))) - (y2 * Math.sin(radians)), (x2 * Math.sin(radians)) + (y2 * Math.cos(radians)));
    lines.push(new Line(point1, point2, theColor));
    x1 = point1.x; y1 = point1.y;
    x2 = point2.x; y2 = point2.y;
  }

  // Recentre the origin back to the middle
  for(let lineLoop=0; lineLoop<lines.length; lineLoop++) {
    lines[lineLoop].p1.x += halfWidth;
    lines[lineLoop].p1.y += halfHeight;
    lines[lineLoop].p2.x += halfWidth;
    lines[lineLoop].p2.y += halfHeight;
  }

  return lines;
}